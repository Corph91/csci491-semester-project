Hey Michael!

So this is it, we've worked together over the past four or so months to polish up and re-do the trivia-cafe website! Here's the install notes:

1.) Make sure you have a local server like WAMP or XAMPP running. This site requires PHP, so if you don't want to install either server be sure to at least install PHP (I personally used v5.6, it ran fine).

2.) Install Composer (PHP package manager).

3.) Install NodeJS next. Use your cli (I prefer Git Bash for this one) to navigate to the directory and run the command "npm install".

4.) Absolutely be sure to install gulp with npm. Doing global (as well as local) install with "npm install -g gulp" is probably a good idea.

5.) In the same director run the command "composer update"

6.) Navigate to localhost/trivia-cafe-website. Be sure that your computer is connected to internet, as some of the site's resources run off of CDN repositories.

7.) Play around and enjoy!

We've all worked really hard on this, and hope you enjoy it.

-SC, Mandoob, Knowlton

NOTE: If you run into any weird WAMP/XAMPP issues, or PHP decides to lose its mind, let us know. Brad is still reachable (though not physically in Missoula), and myself and
      Mandoob will both be here throughout the winter break. I didn't have any hiccups myself.