Trivia Cafe Overhaul

http://www.triviacafe.com

[[TOC]]

# Team Members

Almandub 

PhP /html/java

Bradford Knowlton

Sean Corbett

# Scope of Project

The goal of the trivia cafe overhaul is to provide a long-overdue facelift, with the primary objective of making the site mobile responsive. Secondary objectives include general user interface and presentation enhancements, as well as the rebuilding of the site’s flash trivia game into a mobile-friendly javascript application. The project’s scope is thus wide, however given that much of the required material is already available, completion is not unfeasable.

# Goals

* Provide a general facelift to the website, making the user interface more modern and attractive.

* Make the site mobile-friendly as to increase overall visitor traffic.

* Use a modern design framework to quickly develop responsive layouts.

* Rebuild the site’s Flash game as a platform-agnostic Javascript application

# Development Tools 

## ِAlmandub

Komodo Edit 9 to design

FileZilla Client

Google Chrome on Windows

Iphone 4.s

## Bradford

Coda for php IDE, includes SFTP client

Adobe Photoshop CS

Adobe Illustrator CS

Firefox with Web Developer Toolbar and Firebug

Github UI

SourceTree

## Sean

Atom editor, Intellij Idea

GitBash

FileZilla

Adobe Cloud (all products)

Chrome with Devtools

Samsung Note 7

## General tools

Bitbucket.com for Code repo

Hostgator Web Hosting

Google Drive for collaborative documentation editing

# Timeline

## Project Update

Please provide a document including your team group members.

Due date Friday, September 2, 2016, 11:55 PM

Status: Complete

## Project Update

Please provide a document that outlines your project idea with as much detail as possible including the functional and non-functional specifications for the project.

Due date Friday, September 9, 2016, 11:55 PM

Status: Complete

## Project Update

Please include a document that describe which design tools you are using, which editors you are using and what source code control you are using.

Due date Friday, September 16, 2016, 11:50 PM

Status: Complete

## Project Update

Please provide a document that describes the design of project including pages needed, storyboard of how they will interact, how you will test it.

Due date Friday, September 23, 2016, 11:55 PM

Status: In Progress

## Project Update

Please provide an overall timeline of your project.  Please include a PDF of your Gantt chart.

Due date Friday, September 30, 2016, 11:55 PM

Status: Not Complete

# Site Outline

* Home

    * Question of the Day

    * Questions of the Week

    * Weekly Trivia Contest

    * Submit Your Own Trivia

    * Trivia Categories

        * Animal Trivia

        * Arts Trivia

        * Computer Trivia

        * Currency Trivia

        * Current Event Trivia

        * Education Trivia

        * Entertainment Trivia

        * Fad Trivia

        * General Trivia

        * Geography Trivia

        * Government Trivia

        * History Trivia

        * Labor Trivia

        * Law Trivia

        * Mathematics Trivia

        * Movies Trivia

        * Other Trivia

        * People Trivia

        * Phrase Trivia

        * Politics Trivia

        * Product Trivia

        * Pub Trivia

        * Radio Trivia

        * Religion Trivia

        * Science Trivia

        * Sports Trivia

        * Word Trivia

* About

* Contact

    * Plan Your Own Event

    * Trivia Book Info

    * Buy Trivia

# Testing Plans

## Testing devices

## Testing Procedure

How to verify test devices

Step 1, visit https://www.whatismybrowser.com/ to get screen dimensions, browser and OS

Step 2, add custom url to device details, example result:

http://whatismybrowser.com/w/JKXA6SX

Step 3, visit http://www.speedtest.net/ ** Requires FLASH, skip on mobile devices **

Step 4, add custom url to speed details, example result:

http://www.speedtest.net/my-result/5621156207

## Device List

### iPhone 4

### iPad 3

### iPad 2

### Samsung Note 3

### Cracked Screen Phone

### Sony Laptop

### Segway Gaming Laptop

### Mac Mini

### Macbook Air

#### Device Details

https://www.whatismybrowser.com/

#### Speed Details

http://www.speedtest.net/my-result/5621157741

### Children

### Asus Laptop

### On Campus Lab

### Asus Laptop 15"

#### Device Details

https://www.whatismybrowser.com/w/D3HWDBH

#### Speed Details

http://www.speedtest.net/my-result/5621156207

## Website Testing Tools

### GT Metrix

https://gtmetrix.com/

GTmetrix gives you insight on how well your site loads and provides actionable recommendations on how to optimize it.

### Wave Accessibility Testing

http://wave.webaim.org/

WAVE is developed and made available as a free community service by[ WebAIM](http://webaim.org/). Originally launched in 2001, WAVE has been used to evaluate the accessibility of millions of web pages.

### Optimizely

http://www.optimizely.com

### Crazy Egg

https://www.crazyegg.com/

Find out by seeing how users click and scroll through your website

# Bit Bucket Repo

Bitbucket.org

Website Repo

https://bitbucket.org/csci491/trivia-cafe-website

Game Repo

https://bitbucket.org/csci491/trivia-game

