<?php
global $page_title;
$page_title = "Contact Trivia Cafe";
?>
<?php include('./inc/header.inc.php'); ?>

    <script>
      $('.list-group').hide();
    </script>
    <div class="container">

      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas" id="menu-btn">
              <i class="fa fa-bars" id="menu-toggle"></i>
            </button>
          </p>

           <div id="main-content">
				  <h1>Submit Your Own Trivia Question Trivia Cafe</h1>
                <p>

        <form id="question-form">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Email address</label>
				    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" required>
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">Name</label>
				    <input type="text" class="form-control" id="exampleInputName1" placeholder="Full Name" required pattern=".{5,20}">
				  </div>
                  <div class="form-group">
				    <label for="exampleInputPassword1">Categories</label>
                    <select>
                      <option value="Arts">Arts</option>
                      <option value="Computers">Computers</option>
                      <option value="Education">Education</option>
                      <option value="Mathematics" selected>Mathematics</option>
                      <option value="Phrases">Phrases</option>
                      <option value="Science">Science</option>
                      <option value="Words">Words</option>
                      <option value="Government" selected>Government</option>
                      <option value="History">History</option>
                      <option value="Labor">Labor</option>
                      <option value="Law">Law</option>
                      <option value="Politics" selected>Politics</option>
                      <option value="Religion">Religion</option>
                      <option value="Current Events">Current Events</option>
                      <option value="Entertainment">Entertainment</option>
                      <option value="Fads" selected>Fads</option>
                      <option value="Movies">Movies</option>
                      <option value="Products">Products</option>
                      <option value="Pub">Pub</option>
                      <option value="Radio" selected>Radio</option>
                      <option value="Sports">Sports</option>
                      <option value="Animals">Animals</option>
                      <option value="Currency">Currency</option>
                      <option value="Geography" selected>Geography</option>
                      <option value="People">People</option>

                    </select>
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">Your Own Question</label>
				    <textarea type="text" class="form-control" id="exampleInputQuestion1" placeholder="Your Question?" required pattern=".{10,400}"></textarea>
				  </div>
                  <div class="form-group">
				    <label for="exampleInputPassword1">Your Own Answer</label>
				    <textarea type="text" class="form-control" id="exampleInputAnswer" placeholder="Your Answer." required pattern=".{3,400}"></textarea>
				  </div>

				  <button id="submitBtn" class="btn btn-default btn-primary">Submit</button>
				</form>
                </p>

              </div>



        </div><!--/.col-xs-12.col-sm-9-->

        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
          <?php include('./inc/sidebar.inc.php'); ?>
        </div><!--/.sidebar-offcanvas-->
      </div><!--/row-->

      <hr>

      </div>
      <?php include('./inc/footer.inc.php'); ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo $base_url; ?>/dist/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./dist/js/ie10-viewport-bug-workaround.js"></script>
    <script src="./dist/js/offcanvas.js"></script>
    <script>
      $('#iconified').parent().hide();
      $('.list-group').hide();
      $('.list-group-item2').each(function(){
        $(this).removeClass('active2');
        text = $(this).text();
        if(text == "Submit a Question"){
          $('<i class="fa fa-envelope" style="margin-left: 1em;"></i>').appendTo(this);
          $(this).addClass('active2');
        }
      });

      $('#submitBtn').click(function(){
        var letters = "/^[a-zA-Z\d\-!?.,\s]+$/";
        submit = false;
        $('input, textarea').each(function(){
          if($(this).attr('name') != 'email'){
            text = $(this).val();
            if(text.match(letters)){
              submit = true;
              return true;
            }
            else{
              alert('Please input alphanumeric characters only');
              submit = false;
              return false;
            }

          }
        });
        if(submit){$('#question-form').submit();}
      });
    </script>
  </body>
</html>
