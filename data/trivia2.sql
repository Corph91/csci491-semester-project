-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 19, 2016 at 08:18 PM
-- Server version: 5.5.51-38.2
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `trivcafe_trivcafe`
--

-- --------------------------------------------------------

--
-- Table structure for table `trivia2`
--

CREATE TABLE IF NOT EXISTS `trivia2` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `categoryid` int(3) NOT NULL DEFAULT '0',
  `status` int(3) NOT NULL DEFAULT '0',
  `timeactive` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trivia2`
--

INSERT INTO `trivia2` (`id`, `question`, `answer`) VALUES
(51, 'The product became a staple on the television show "The Pretender."', 'PEZ'),
(57, 'This region was a kingdom of Czechoslovakia until the 15th century, when it passed to Hungary and then to the Hapsburgs. It became the core of the new state of Czechoslovakia in 1918. Today the name can refer to certain artistic intellectuals. Which regio', 'BOHEMIA'),
(50, 'Based on average income, what is the poorest nation in the Western Hemisphere?', 'Haiti'),
(41, 'Which U.S. city has these three nicknames: air crossroads of the world, Chicago of the north, largest city in the largest state?', 'Anchorage Alaska'),
(40, 'What were the names of these two popular films: the remake from 1991 of the original film from 1950, with the same title, which starred Spencer Tracy and Elizabeth Taylor as father and daughter. What''s the title?', 'Father of the bride'),
(59, 'Absolute zero registers at how many degrees Celsius?', '-273°C'),
(55, 'Because of the population problem, in China, married couples lose various governmental benefits after the birth of which child?', 'SECOND'),
(20, 'This one time Ivory Soap box model starred in the X-rated film, Behind the Green Door. Who is it?', 'MARILYN CHAMBERS'),
(42, 'During the 1980''s which entertainer was considered to be the poet laureate of Colorado?', 'John Denver'),
(47, 'Edward Teach, an Englishman who turned to an extremely savage form of piracy, was better known by what colorful name?', 'BlackBeard'),
(60, 'If the telephone company added one more digit to all the phone numbers in one area code, how many new phone numbers could potentially be added?', '90,000,000'),
(56, 'In 1958, Russian poet and novelist Boris Pasternak was awarded the Nobel Prize for literature, but negative Soviet reaction forced him to refuse the honor. For which novel was Boris Pasternak awarded the Nobel Prize?', 'DR. ZHIVAGO'),
(63, 'The British Union Jack can be found in the flag of which US state?', 'HAWAII'),
(58, 'Which game is played on a marked board divided into two halves, each side containing 12 alternately colored points?', 'BACKGAMMON'),
(45, 'The Parthenon was built around 440 B.C.  a. To which goddess was the temple dedicated? B. On which hill is it located?', 'A. Athena B. Acropolis'),
(65, 'Many military leaders have gone on to rule a country. This person waged a personal battle against a superior enemy, then went on to rule his country, around three thousand years ago. Who was he?', 'King David'),
(54, 'What was the only part of the current United States invaded by the Japanese during WWII?', 'Alaska'),
(64, 'The most expensive music manuscripts ever sold at auction fetched $3.8 million in 1987 at Sotheby''s in London. Who was the composer of this  music?', 'MOZART'),
(62, 'On Dec. 10, 1953, the first Playboy magazine was released. Which well-known woman was the first playmate?', 'MARILYN MONROE'),
(44, 'If a diamond is the hardest known substance , what is used to cut diamonds?', 'Diamonds'),
(53, 'The Sine of an angle is ....', 'The ratio of the length of the opposite side to the length of the hypoteneuse.'),
(52, 'TV show about a genius who can become anyone that he wants to be.', 'The Pretender'),
(61, 'Which plant, common around Christmas time, contains poisonous berries which can cause acute stomach irritation if eaten?', 'MISTELTOE'),
(46, 'What is the name for the type of Japanese verse form having three lines with 5, 7, and 5 syllables?', 'Haiku'),
(48, 'The first talking picture in Britain was called Blackmail. It was made in 1929, and was directed by which well-known director?', 'Alfred Hitchcock'),
(49, 'Draw a circle. Draw the radius. Make a square out of one of the radii. How many of those squares do you think would fit inside the circle?', 'Exactly PI Number of them');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `trivia2`
--
ALTER TABLE `trivia2`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `trivia2`
--
ALTER TABLE `trivia2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
