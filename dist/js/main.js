// http://stackoverflow.com/questions/3718282/javascript-shuffling-objects-inside-an-object-randomize
$(document).ready(function(){
    $('body').show();
   placeholder = $('#iconified').attr('placeholder');
   var wasactive = null;
   var $window = $(window);
   var $pane = $('#pane1');
   function checkWidth() {
       var windowsize = $window.width();
       return (windowsize < 1000) ? true : false;
   }
   // Execute on load
   var smallwindow = checkWidth();
   // Bind event listener
   $('#menu-btn').click(function(){
     if(smallwindow){
       if($('footer').is(":visible")){
         $('footer').fadeOut(100,'linear');
         $('footer').css('height','0px');
       }
       else {
         $('footer').fadeIn(250,'linear');
         $('footer').css('height','100%');
       }
     }
   });

   $(window).resize(checkWidth);
   $location = window.location.href;

  function shuffle(sourceArray) {
      for (var i = 0; i < sourceArray.length - 1; i++) {
          var j = i + Math.floor(Math.random() * (sourceArray.length - i));

          var temp = sourceArray[j];
          sourceArray[j] = sourceArray[i];
          sourceArray[i] = temp;
      }
      return sourceArray;
  }

  // http://www.w3schools.com/js/js_cookies.asp

  function getCookie(cname) {
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for(var i = 0; i <ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0)==' ') {
              c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
              return c.substring(name.length,c.length);
          }
      }
      return "";
  }

  var Questions;
  var cat_count = {};
  var newQuestion = function(category = 0){

    $( "#questions" ).html('');

    if( Questions.length == 0){

      $( "#questions" ).html("Out of Questions, try again later");

    }else{
      item = Questions.shift();

      header = "<h4 style='letter-spacing: 0.15em;'>trivia-izer:</i></h4>";
      question = "<h5 id='" + item.id + "' class='Question'>"
      + item.question + "</h5>";

      showAnswer = "<input type='submit' name='submit' value='answer' class='btn btn-primary' id='answer-btn'/>";
      nextQuestion = "<button class='btn btn-primary' id='next-trivia'>next question</button>";
      triviaNav = "<div class='trivia-nav'></div>";
      socialMedia = '<i class="fa fa-share-alt" id="share-menu"></i><ul id="media-menu"><li id="first"><i class="fa fa-facebook"></i></li><li><i class="fa fa-twitter"></i></li><li><i class="fa fa-google"></i></li><li><i class="fa fa-tumblr"></i></li></ul>';

       $('#questions').append(header).append(question).append(triviaNav);
       $('.Question').css('height','1em').text('');
       $('.trivia-nav').css('margin-top','6em');
       $(showAnswer).appendTo('.trivia-nav').css('outline','0').css('margin-right','2em').css('pointer-events','auto');
       $(nextQuestion).appendTo('.trivia-nav').css('outline','0');
       $(socialMedia).appendTo('.trivia-nav').css('list-style','none').css('display','inline');
       $('#media-menu').hide();
       setTimeout(function(){
         $('.Question').attr('id',item.id).text(item.question);
       },100);
       $('#answer-btn').fadeTo(250,1).css('pointer-events','auto');

       $('#answer-btn').click(function(){
        $('#answer-btn').fadeTo(250,0.1).css('pointer-events','none');
        $('.Question').attr('id',item.id).text(item.answer);

       });
       $location = window.location.href;
    }
  }

  $(document).on('click','#next-trivia',function(e){
    e.preventDefault();
      newQuestion();
  });

  var Categories;
  function buildCategories(){
    var items = [];

    $.each( Categories, function( i, category ) {
      items.push( "<li id='" + i + "'><a href='#' class='btn btn-primary' onclick='newQuestion("+category.id+");return false;'>" + category.name + "(" +cat_count[category.id]+ ")</a><br/>"+category.description+"</li>" );
    });
  }

  buildCategories();

  var oldQuestions = Questions;
  //initialize after images are loaded
  $('.cat-tab').hide();
  $.getJSON( "./data/categories.json", function(data) {
       // Questions = shuffle(data);
      Categories = data;
      buildCategories();

      $('.cat-tab').each(function(){
        $(this).find('li').each(function(){
          item = $(this);
          text = item.text();
          $.each(Categories,function(i){;
            if(text == Categories[i].name) {
              itemTxt = item.attr('class');
              item.attr('class',itemTxt +' '+ Categories[i].id);
            }
          });
          item.text(text+": ");
        }).removeClass('undefined');
      });
    });

  $.getJSON( "./data/trivia-bigger-test.json", function(data) {
     // Questions = shuffle(data);
     Questions = shuffle(data);
     cat_count = {};

     $.each(Questions,function(i){
       key = Questions[i].categoryid;
       if(typeof(cat_count[key]) == "undefined"){
          cat_count[key] = 0;
       }
       else{
         cat_count[key] ++;
       }
     });

    $('.list-group-item2').click(function(){
      $('.list-group-item2').each(function(){
        $(this).removeClass('active2')
      });
      $(this).addClass('active2');
    });

    $('.cat-tab li').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      id = $(this).attr('class');
      newQuestions = [];
      Questions = data;
      for(j=0; j<Questions.length; j++){
        if(Questions[j].categoryid == id){
          if($.inArray(Questions[j], newQuestions) === -1){
            newQuestions.push(Questions[j]);
          }
        }
      }

      Questions = newQuestions;
      newQuestion();

    });

    $('#iconified').keypress(function(e){
      if(e.which == 13) {
        matches = 0;
        text = $('#iconified').val();
        newQuestions = [];
        Questions = data;
        for(j=0; j<Questions.length; j++){
          if(Questions[j].question.includes(text)){
            if($.inArray(Questions[j], newQuestions) === -1){
              newQuestions.push(Questions[j]);
              matches += 1;
            }
          }
        }
        wasactive = $('.active');
        wasactive.removeClass('active').css('pointer-events','auto').find('i').hide();
        $('#iconified').val("found: "+matches).css('color','rgba(0,0,0,0.6)');
        Questions = newQuestions;
        newQuestion();
      }
    });

   $('.list-group-item').click(function(e){
     e.preventDefault();
     $('a').removeClass('active')
     $('.list-group-item').css('pointer-events','auto');
     $('.cat-tab').slideUp(500);
     $(this).addClass('active').css('pointer-events','none').find('.cat-tab').slideDown(500);
     if(!$(this).text().includes("All")){
       newQuestions = [];
       Questions = data;
       item = $(this).find('li');
       item.each(function(){
         for(j=0; j<Questions.length; j++){
           current = $(this);
           id = current.attr('class');
           if(Questions[j].categoryid == id){
             if($.inArray(Questions[j], newQuestions) === -1){
               newQuestions.push(Questions[j]);
             }
           }
         }
         //item.text(text+cat_count[key]);1
         current = $(this);
         text = current.text().replace(/[0-9]/g, '');
         current.text(text +': '+0);
         for(key in cat_count){
           if(key == current.attr('class')){
             text = text + cat_count[key];
             current.text(text);
           }
         }
       });
       Questions = newQuestions;
       newQuestion();
     }
     else {
       Questions = data;
       newQuestion();
     }
   });
   $('#all').trigger("click");
  });

  $('.nav li').click(function(){
    $('.nav li').removeClass('active');
    $(this).addClass('active');
  });
  $('.cat-tab li').each(function(){
    $(this).css('pointer-events','auto');
  });

  $('#menu-toggle').mouseenter(function(){
    $('#menu-toggle').addClass('animated flash');
  }).mouseleave(function(){
    setTimeout(function(){
      $('#menu-toggle').removeClass('animated flash');
    },500);
  });
  $('.cat-tab li').click(function(){
    $('.cat-tab').each(function(){
      $(this).find('li').each(function(){
        $(this).children().remove('i');
      });
    });
    $('<i class="fa fa-check-circle-o" style="margin-left: 1em;"></i>').appendTo(this);
  });

  $('.list-group-item').click(function(){
    $('.list-group-item p, .list-group-item').each(function(){
      $(this).children().remove('i');
    });
    appendItem = $(this).find('p');
    $('<i class="fa fa-list-ul" style="margin-left: 1em;"></i>').appendTo(appendItem);
  });

  $(document).on('click','#share-menu',function(){
    if(!$('#media-menu').is(':visible')){
      $('#media-menu').show().addClass('animated fadeInLeft');
    }else {
      setTimeout(function(){
        $('#media-menu').removeClass('fadeInLeft').addClass('fadeOutLeft');
        setTimeout(function(){
          $('#media-menu').removeClass('animated fadeOutLeft').hide();
        },1000);
      },100);
    }
  });
  /*
   * Social media parsing doesn't work on a local server,
   * so we opted for simple links that can easily be modified
   * should the site ever be deployed on an actual server.
   */
  $(document).on('click','#media-menu li',function(){
    if($(this).find('i').attr('class').includes("fa-facebook")){
      window.location.replace('http://www.facebook.com');
    }
    else if($(this).find('i').attr('class').includes("fa-google")){
      window.location.replace('http://plus.google.com');
    }
    else if($(this).find('i').attr('class').includes("fa-twitter")){
      window.location.replace('http://www.twitter.com');
    }
    else if($(this).find('i').attr('class').includes("fa-tumblr")){
      window.location.replace('http://www.tumblr.com');
    }
  });

  $('.list-group-item').click(function(){
    input =  $('#iconified');
    if(input.val().length > 0) {
      input.val('');
      input.attr('placeholder',placeholder);
      input.addClass('empty');
    }
  });

  $('#iconified').focus(function(){
    var input = $(this);
    if(input.val().length === 0) {
        input.attr('placeholder',placeholder);
        input.addClass('empty');
    } else {
        input.val('');
        input.attr('placeholder','');
        input.removeClass('empty');
    }
    wasactive = $('.active');
    wasactive.removeClass('active').css('pointer-events','auto').find('i').hide();
  }).blur(function(){
    var input = $(this);
    input.val('');
    input.attr('placeholder',placeholder);
    input.addClass('empty');
    wasactive.addClass('active').css('pointer-events','none').find('i').show();
  });
  $('#iconified').on('keyup', function() {
    var input = $(this);
    if(input.val().length === 0) {
        input.addClass('empty');
    } else {
        input.removeClass('empty');
    }
});


});
