<?php
global $page_title;
$page_title = "Contact Trivia Cafe";
?>
<?php include('./inc/header.inc.php'); ?>

    <script>
      $('.list-group').hide();
    </script>
    <div class="container">

      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>
          <div class="jumbotron">
            <h1>Trivia Cafe</h1>
            <p>Test you knowledge.</p>
          </div>
          <div class="row">
            <div class="col-xs-12">

              <div id="questions">

                LOADING....

              </div>

            </div><!--/.col-xs-6.col-lg-4-->
          </div><!--/row-->
        </div><!--/.col-xs-12.col-sm-9-->

        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
          <?php include('./inc/sidebar.inc.php'); ?>
        </div><!--/.sidebar-offcanvas-->
      </div><!--/row-->

      <hr>

      <footer ><!--  -->
        <div class="row">
  <div class="col-md-12 text-center" ><i >"Have You Had Your Trivia Today?"</i></div>
  <div class="col-md-12 text-center"  ><a href="#" align=>www.triviacafe.com</a>© 2005-2016</div>
        <div class="col-md-12 text-center" align="center" >Howard Rachelson - Contact: howard1@triviacafe.com</div>

        </div>
        </div>
      </footer>

    </div><!--/.container-->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="./dist/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./dist/js/ie10-viewport-bug-workaround.js"></script>
    <script src="./dist/js/offcanvas.js"></script>
    <script>
      $('.list-group-item2').each(function(){
        $(this).removeClass('active2');
        text = $(this).text();
        if(text == "Help"){
          $(this).addClass('active2');
        }
      });
    </script>
  </body>
</html>
