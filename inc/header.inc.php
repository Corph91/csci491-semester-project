<?php include('global.inc.php'); ?>
<!DOCTYPE html>
<html lang="en">

  <?php include('head.inc.php'); ?>
  <?php global $body_class; ?>
  <body class="<?php echo join(' ',$body_class);?>">

  <header>
    <div class="container">
      <div class="row" id="first-row">

      <div class="col-xs-12 col-sm-8" style="border-bottom: thin solid #333; margin-bottom: 0.5em;">
        <h1 style="letter-spacing: 0.15em;">trivia cafe</h1>
        <h4><em>Have you had your trivia today?</em></h4>
      </div>
      <div class="col-sm-1 hidden-xs">
      </div>
      <div class="col-sm-3 hidden-xs" style="margin-top: 0.5em;">
        <img src="<?php echo $base_url; ?>dist/img/cross-legged-reader.png" style="width: 20%;"/>
      </div>


    </div>
    <div class="row" style="margin-bottom: 1em;">
      <div class="col-sm-9 hidden-xs">
      </div>
      <div class="col-sm-3 hidden-xs">
        <input type="text" class="form-control empty" id="iconified" placeholder="&#xF002;"/>
      </div>
    </div><!-- /.row -->
    </div><!-- /.container -->
</header>
