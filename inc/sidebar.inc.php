<link href="<?php echo $base_url; ?>/dist/css/sidebar.css" rel="stylesheet">
<div class="list-group2">
	<a role="presentation" style="border-top-left-radius: 4px; border-top-right-radius: 4px;" class="list-group-item2" href="<?php echo $base_url; ?>">Home</a>
	<a role="presentation" class="list-group-item2" href="<?php echo $base_url; ?>about.php">About</a>
	<a role="presentation" class="list-group-item2" href="<?php echo $base_url; ?>contact.php">Contact</a>
	<a role="presentation" class="list-group-item2" href="<?php echo $base_url; ?>YourOwnQuestion.php">Submit a Question</a>
	<a role="presentation" class="list-group-item2" href="<?php echo $base_url; ?>contest.php">Weekly Trivia Contest</a>
	<a role="presentation" class="list-group-item2" href="<?php echo $base_url; ?>questions.php">Questions of the Week</a>
	<a role="presentation" style="border-bottom-left-radius: 4px; border-bottom-right-radius: 4px;" class="list-group-item2" href="<?php echo $base_url; ?>question.php">Question of the Day</a>

</div>

<div class="list-group">
            <a href="#" class="list-group-item active" id="all">
							<p>All</p>
						</a>
            <a href="#" class="list-group-item">
              <p>Art, Language, and Numbers</p>
              <ul class="cat-tab" id="cat-tab-1">
                <li id="art">Arts</li>
                <li id="computer">Computers</li>
                <li id="education">Education</li>
                <li id="mathematics">Mathematics</li>
                <li id="phrase">Phrases</li>
                <li id="science">Science</li>
                <li id="word">Words</li>
              </ul>
            </a>
            <a href="#" class="list-group-item">
              <p>Civics, History, and Religion</p>
              <ul class="cat-tab" id="cat-tab-2">
                <li id="government">Government</li>
                <li id="history">History</li>
                <li id="labor">Labor</li>
                <li id="law">Law</li>
                <li id="politics">Politics</li>
                <li id="religion">Religion</li>
              </ul>
            </a>
            <a href="#" class="list-group-item">
              <p>Culture and Current Events</p>
              <ul class="cat-tab" id="cat-tab-3">
                <li id="current-events">Current Events</li>
                <li id="entertainment">Entertainment</li>
                <li id="fads">Fads</li>
                <li id="movies">Movies</li>
                <li id="products">Products</li>
                <li id="pub">Pub</li>
                <li id="radio">Radio</li>
                <li id="sports">Sports</li>
              </ul>
            </a>
            <a href="#" class="list-group-item">
              <p>The World About Us</p>
              <ul class="cat-tab" id="cat-tab-4">
                <li id="animals">Animals</li>
                <li id="currency">Currency</li>
                <li id="geography">Geography</li>
                <li id="people">People</li>
              </ul>
            </a>
          </div>
