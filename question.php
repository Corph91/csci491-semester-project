<?php
global $page_title;
$page_title = "Question of the Day! Welcome To Trivia Cafe";
global $body_class;
$body_class[] = "single-question";
?>
<?php include('./inc/header.inc.php'); ?>

    <div class="container">

      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas" id="menu-btn">
              <i class="fa fa-bars" id="menu-toggle"></i>
            </button>
          </p>
          <div class="jumbotron">
            <h1>daily question</h1>
            <p><em>for today only!</em></p>
          </div>
          <div class="row">
            <div class="col-xs-12">

              <div id="questions">

                LOADING....

              </div>

            </div><!--/.col-xs-6.col-lg-4-->
          </div><!--/row-->
        </div><!--/.col-xs-12.col-sm-9-->

        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
          <?php include('./inc/sidebar.inc.php'); ?>
        </div><!--/.sidebar-offcanvas-->
      </div><!--/row-->

      <hr>
    </div>
    <?php include('./inc/footer.inc.php'); ?>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="./dist/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./dist/js/ie10-viewport-bug-workaround.js"></script>
    <script src="./dist/js/offcanvas.js"></script>
    <script>
      $('#iconified').parent().hide();
      $('.list-group').hide();
      $('.list-group-item2').each(function(){
        $(this).removeClass('active2');
        text = $(this).text();
        if(text == "Question of the Day"){
          $('<i class="fa fa-cube" style="margin-left: 1em;"></i>').appendTo(this);
          $(this).addClass('active2');
        }
      });
    </script>
  </body>
</html>
